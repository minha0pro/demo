Add ssh-key to your remote repository (github, gitlab, bitbucket)
Change to jenkins user
$ sudo su - jenkins

Generate ssh-key
$ ssh-gen 

Copy ~/.ssh/id_rsa.pub to your remote

If jenkins cannot clone your code
$ sudo su - jenkins

Add remote to host key verified
$ git ls-remote -h git@bitbucket.org:person/projectmarket.git HEAD


Make jenkins have permission to run docker

$ sudo usermod -aG docker jenkins
 
 